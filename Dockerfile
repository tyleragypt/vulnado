FROM openjdk:8

RUN apt-get update && \
    apt-get install build-essential maven default-jdk cowsay netcat -y && \
    update-alternatives --config javac
COPY . .

# Execution of example command. Here it is used to show a list of files and directories.
# It will be useful in later exercises in this tutorial. 
RUN ls -list
CMD ["mvn install"]

# To execute sonar-scanner we just need to run "sonar-scanner" in the image. 
# To pass Sonarqube parameter we need to add "-D"prefix to each as in the example below
# sonar.host.url is property used to define URL of Sonarqube server
# sonar.projectKey is used to define project key that will be used to distinguish it in 
# sonarqube server from other projects
# sonar.sources directory for sources of project
RUN sonar-scanner \
  -Dsonar.projectKey=Vulnado \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=71133ae2ebf9aef85f15666140ab0e3a57229087 \
  -Dsonar.java.binaries=target \
  -Dsonar.sources=src/main/java/
CMD ["mvn", "spring-boot:run"]
